-- 1. Create a new user with the username "rentaluser" and the password "rentalpassword". Give the user the ability to connect to the database but no other permissions.
-- Check if the user exists and create if it doesn't
DO $$
BEGIN
   IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'rentaluser') THEN
      CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
   ELSE
      ALTER USER rentaluser WITH PASSWORD 'rentalpassword';
   END IF;
END
$$;

-- Grant connection privileges on the database
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- Optionally, revoke all other privileges
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM rentaluser;
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM rentaluser;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM rentaluser;



---2. Grant "rentaluser" SELECT permission for the "customer" table. Сheck to make sure this permission works correctly—write a SQL query to select all customers.
GRANT SELECT ON TABLE public.customer TO rentaluser;

SELECT * FROM public.customer;


--- 3. Create a new user group called "rental" and add "rentaluser" to the group. 
-- Create a new user group called "rental"
CREATE ROLE rental;

-- Add "rentaluser" to the "rental" group
GRANT rental TO rentaluser;
-- Check if "rentaluser" is part of the "rental" group
SELECT grantee, role_name 
FROM information_schema.role_table_grants 
WHERE grantee = 'rentaluser';
 
--- 4. Grant the "rental" group INSERT and UPDATE permissions for the "rental" table. Insert a new row and update one existing row in the "rental" table under that role. 

-- Grant INSERT and UPDATE permissions on the "rental" table to the "rental" group
GRANT INSERT, UPDATE ON TABLE public.rental TO rental;
-- Insert a new row into the "rental" table
INSERT INTO public.rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES ('2023-06-25 10:00:00', 1, 1, '2023-06-26 10:00:00', 1, NOW());

-- Update an existing row in the "rental" table
UPDATE public.rental
SET return_date = '2023-06-26 11:00:00', last_update = NOW()
WHERE rental_id = 1;


--- 5. Revoke the "rental" group's INSERT permission for the "rental" table. Try to insert new rows into the "rental" table make sure this action is denied.
-- Revoke INSERT permission on the "rental" table from the "rental" group
REVOKE INSERT ON TABLE public.rental FROM rental;
-- Attempt to insert a new row into the "rental" table (should fail)
INSERT INTO public.rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES ('2023-06-25 10:00:00', 1, 1, '2023-06-26 10:00:00', 1, NOW());




--- 6. Create a personalized role for any customer already existing in the dvd_rental database. The name of the role name must be client_{first_name}_{last_name} (omit curly brackets). The customer's payment and rental history must not be empty. Configure that role so that the customer can only access their own data in the "rental" and "payment" tables. Write a query to make sure this user sees only their own data.
-- Identify a customer with non-empty payment and rental history
SELECT c.customer_id, c.first_name, c.last_name
FROM public.customer c
JOIN public.payment p ON c.customer_id = p.customer_id
JOIN public.rental r ON c.customer_id = r.customer_id
GROUP BY c.customer_id, c.first_name, c.last_name
HAVING COUNT(p.payment_id) > 0 AND COUNT(r.rental_id) > 0
LIMIT 1;

-- Create personalized role for the identified customer
CREATE ROLE client_Mary_Smith WITH LOGIN PASSWORD 'securepassword';
GRANT CONNECT ON DATABASE dvdrental TO client_Mary_Smith;
GRANT SELECT ON TABLE public.rental TO client_Mary_Smith;
GRANT SELECT ON TABLE public.payment TO client_Mary_Smith;

--Enable row-level security and create policies
ALTER TABLE public.rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE public.payment ENABLE ROW LEVEL SECURITY;

CREATE POLICY rental_policy ON public.rental
USING (customer_id = current_setting('myapp.user_id')::integer);

CREATE POLICY payment_policy ON public.payment
USING (customer_id = current_setting('myapp.user_id')::integer);

-- Set the user_id for the personalized role
ALTER ROLE client_Mary_Smith SET myapp.user_id = '1';

-- Verify the setup (to be run under client_Mary_Smith session)
SELECT * FROM public.rental;
SELECT * FROM public.payment;







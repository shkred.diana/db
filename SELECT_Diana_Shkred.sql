--Writing Queries
SELECT COUNT(*)
FROM film f
LEFT JOIN inventory i ON f.film_id = i.film_id
LEFT JOIN rental r ON i.inventory_id = r.inventory_id
WHERE r.inventory_id IS NULL;

SELECT f.title, SUM(p.amount) AS total_income
FROM film f
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
JOIN payment p ON r.rental_id = p.rental_id
GROUP BY f.film_id, f.title
ORDER BY total_income ASC
LIMIT 3;

SELECT
    s.store_id,
    SUM(p.amount) AS total_revenue
FROM
    payment p
JOIN
    rental r ON p.rental_id = r.rental_id
JOIN
    inventory i ON r.inventory_id = i.inventory_id
JOIN
    store s ON i.store_id = s.store_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id
ORDER BY
    total_revenue DESC
LIMIT 1;



--Q1, solution 1

SELECT
    s.store_id,
    st.staff_id,
    st.first_name,
    st.last_name,
    SUM(p.amount) AS total_revenue
FROM
    payment p
JOIN
    rental r ON p.rental_id = r.rental_id
JOIN
    inventory i ON r.inventory_id = i.inventory_id
JOIN
    store s ON i.store_id = s.store_id
JOIN
    staff st ON r.staff_id = st.staff_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id, st.staff_id, st.first_name, st.last_name
ORDER BY
    s.store_id, total_revenue DESC;
   
--q1, solution 2
WITH RevenuePerStaff AS (
    SELECT
        s.store_id,
        st.staff_id,
        st.first_name,
        st.last_name,
        SUM(p.amount) AS total_revenue,
        ROW_NUMBER() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
    FROM
        payment p
    JOIN
        rental r ON p.rental_id = r.rental_id
    JOIN
        inventory i ON r.inventory_id = i.inventory_id
    JOIN
        store s ON i.store_id = s.store_id
    JOIN
        staff st ON r.staff_id = st.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, st.staff_id, st.first_name, st.last_name
)
SELECT
    store_id,
    staff_id,
    first_name,
    last_name,
    total_revenue
FROM
    RevenuePerStaff
WHERE
    revenue_rank = 1
ORDER BY
    store_id;
   
   

 --q2
   
WITH RentalCounts AS (
    SELECT
        f.film_id,
        f.title,
        COUNT(*) AS rental_count
    FROM
        film f
    JOIN
        inventory i ON f.film_id = i.film_id
    JOIN
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id, f.title
    ORDER BY
        rental_count DESC
    LIMIT 5
),
CustomerAges AS (
    SELECT
        r.inventory_id,
        EXTRACT(YEAR FROM AGE(c.create_date)) AS age
    FROM
        rental r
    JOIN
        customer c ON r.customer_id = c.customer_id
)
SELECT
    rc.title,
    rc.rental_count,
    AVG(ca.age) AS average_age
FROM
    RentalCounts rc
JOIN
    inventory i ON rc.film_id = i.film_id
JOIN
    rental r ON i.inventory_id = r.inventory_id
JOIN
    CustomerAges ca ON r.inventory_id = ca.inventory_id
GROUP BY
    rc.title, rc.rental_count
ORDER BY
    rc.rental_count DESC;
   

--q2, solution 2   
SELECT
    f.title,
    COUNT(r.rental_id) AS rental_count
FROM
    rental r
    JOIN inventory i ON r.inventory_id = i.inventory_id
    JOIN film f ON i.film_id = f.film_id
GROUP BY
    f.title
ORDER BY
    rental_count DESC
LIMIT 5;

WITH top_movies AS (
    SELECT
        f.film_id,
        f.title,
        COUNT(r.rental_id) AS rental_count
    FROM
        rental r
        JOIN inventory i ON r.inventory_id = i.inventory_id
        JOIN film f ON i.film_id = f.film_id
    GROUP BY
        f.film_id,
        f.title
    ORDER BY
        rental_count DESC
    LIMIT 5
)

SELECT
    tm.title,
    AVG(EXTRACT(YEAR FROM AGE(c.create_date))) AS average_age
FROM
    top_movies tm
    JOIN inventory i ON tm.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    JOIN customer c ON r.customer_id = c.customer_id
GROUP BY
    tm.title;

   
--q3

WITH ActorFilmDates AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        f.release_year,
        ROW_NUMBER() OVER (PARTITION BY a.actor_id ORDER BY f.release_year DESC) AS rn
    FROM
        actor a
    JOIN
        film_actor fa ON a.actor_id = fa.actor_id
    JOIN
        film f ON fa.film_id = f.film_id
),
ActorLastFilms AS (
    SELECT
        actor_id,
        first_name,
        last_name,
        release_year,
        LEAD(release_year) OVER (PARTITION BY actor_id ORDER BY release_year DESC) AS previous_film_year
    FROM
        ActorFilmDates
    WHERE
        rn <= 2
),
ActorGaps AS (
    SELECT
        actor_id,
        first_name,
        last_name,
        release_year - previous_film_year AS years_between_films
    FROM
        ActorLastFilms
    WHERE
        previous_film_year IS NOT NULL
)
SELECT
    actor_id,
    first_name,
    last_name,
    years_between_films
FROM
    ActorGaps
WHERE
    years_between_films = (SELECT MAX(years_between_films) FROM ActorGaps)
ORDER BY
    actor_id;

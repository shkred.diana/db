--Remove a previously inserted film from the inventory and all corresponding rental records

DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = (SELECT film_id FROM film WHERE title = 'Interstellar')
);

DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'Interstellar');

-- Remove Records Related to "Diana Shkred" from All Tables Except "Customer" and "Inventory"
DELETE FROM rental
WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Diana' AND last_name = 'Shkred');

DELETE FROM payment
WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Diana' AND last_name = 'Shkred');
-- 1. Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.
UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'Interstellar';

/*2. Alter any existing customer in the database with at least 10 rental and 10 payment records. 
Change their personal data to yours (first name, last name, address, etc.). You can use any existing address from the "address" table. 
Please do not perform any updates on the "address" table, as this can impact multiple records with the same address.*/

UPDATE customer
SET first_name = 'Diana', 
    last_name = 'Shkred',
    email = 'diana_s@gmail.com', 
    address_id = (SELECT address_id FROM address LIMIT 1)
WHERE customer_id = (
    SELECT rental_customers.customer_id
    FROM (
        SELECT customer_id
        FROM rental
        GROUP BY customer_id
        HAVING COUNT(*) >= 10
    ) AS rental_customers
    INNER JOIN (
        SELECT customer_id
        FROM payment
        GROUP BY customer_id
        HAVING COUNT(*) >= 10
    ) AS payment_customers
    ON rental_customers.customer_id = payment_customers.customer_id
    LIMIT 1
);



-- 3. Change the customer's create_date to current_date
UPDATE customer
SET create_date = CURRENT_DATE;
  


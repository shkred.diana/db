--I've changed my the 3nf model a bit. I changed the data type for the id and changed the column names for easy writing


CREATE SCHEMA climbing_data;
-- Create Climbers table
CREATE TABLE climbing_data.climbers (
    climber_id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    city VARCHAR(255) NOT NULL,
    state VARCHAR(255) NOT NULL,
    postal_code INT NOT NULL CHECK (postal_code > 0),
    country VARCHAR(255) NOT NULL
);

-- Create Mountains table
CREATE TABLE climbing_data.mountains (
    mountain_id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    height INT CHECK (height > 0),
    country VARCHAR(255) NOT NULL,
    region VARCHAR(255) NOT NULL
);

-- Create Routes table
CREATE TABLE climbing_data.routes (
    route_id SERIAL PRIMARY KEY,
    FOREIGN KEY (mountain_id) REFERENCES climbing_data.mountains(mountain_id),
    name VARCHAR(255) NOT NULL,
    difficulty VARCHAR(255) NOT NULL
);

-- Create Climbs table
CREATE TABLE climbing_data.climbs (
    climb_id SERIAL PRIMARY KEY,
    start_date DATE NOT NULL CHECK (start_date > '2000-01-01'),
    end_date DATE CHECK (end_date > start_date)
);

-- Create ClimberClimbs junction table
CREATE TABLE climbing_data.climber_climbs (
    FOREIGN KEY (climber_id) REFERENCES climbing_data.climbers(climber_id),
    FOREIGN KEY (climb_id) REFERENCES climbing_data.climbs(climb_id),
    PRIMARY KEY (climber_id, climb_id)
);

-- Create Equipment table
CREATE TABLE climbing_data.equipment (
    equipment_id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    weight DECIMAL CHECK (weight > 0)
);

-- Create ClimbEquipment junction table
CREATE TABLE climbing_data.climb_equipment (
    FOREIGN KEY (climb_id) REFERENCES climbing_data.climbs(climb_id),
    FOREIGN KEY (equipment_id) REFERENCES climbing_data.equipment(equipment_id),
    quantity INT NOT NULL CHECK (quantity > 0),
    PRIMARY KEY (climb_id, equipment_id)
);

-- Create MembershipFees table
CREATE TABLE climbing_data.membership_fees (
    fee_id SERIAL PRIMARY KEY,
    FOREIGN KEY (climber_id) REFERENCES climbing_data.climbers(climber_id),
    fee_date DATE NOT NULL CHECK (fee_date > '2000-01-01'),
    amount DECIMAL CHECK (amount > 0),
    payment_method VARCHAR(255) NOT NULL
);

-- Create ClimberAchievements table
CREATE TABLE climbing_data.climber_achievements (
    achievement_id SERIAL PRIMARY KEY,
    FOREIGN KEY (climber_id) REFERENCES climbing_data.climbers(climber_id),
    achievement_date DATE NOT NULL CHECK (achievement_date > '2000-01-01'),
    title VARCHAR(255) NOT NULL,
    description TEXT,
    awarded_for VARCHAR(255)
);

-- Create ExpeditionPhotos table
CREATE TABLE climbing_data.expedition_photos (
    photo_id SERIAL PRIMARY KEY,
    FOREIGN KEY (climb_id) REFERENCES climbing_data.climbs(climb_id),
    photo_url VARCHAR(255) NOT NULL,
    taken_by VARCHAR(255),
    taken_date DATE,
    description TEXT
);

-- Create ClimbRoutes junction table
CREATE TABLE climbing_data.climb_routes (
    FOREIGN KEY (climb_id) REFERENCES climbing_data.climbs(climb_id),
    FOREIGN KEY (route_id) REFERENCES climbing_data.routes(route_id),
    PRIMARY KEY (climb_id, route_id)
);



-- Adding a duration_days column
ALTER TABLE club_data.climbs
ADD COLUMN climb_duration_days INT GENERATED ALWAYS AS (end_date - start_date) STORED;


-- Adding a check constraint for date and unique constraint
ALTER TABLE climbing_data.climbs
ADD CONSTRAINT chk_start_date CHECK (start_date > '2000-01-01'),
	
ALTER TABLE climbing_data.mountains
ADD CONSTRAINT unique_mountain_name UNIQUE (name);

ALTER TABLE climbing_data.climber_achievements
ADD CONSTRAINT chk_achievement_date CHECK (achievement_date > '2000-01-01');

ALTER TABLE climbing_data.membership_fees
ADD CONSTRAINT chk_fee_date CHECK (fee_date > '2000-01-01');



-- Check constraint for non-negative values for heights and weights
ALTER TABLE climbing_data.mountains
ADD CONSTRAINT chk_height_positive CHECK (height > 0);

ALTER TABLE climbing_data.equipment
ADD CONSTRAINT chk_weight_non_negative CHECK (weight > 0);

ALTER TABLE climbing_data.climb_equipment
ADD CONSTRAINT chk_quantity_positive CHECK (quantity > 0);

ALTER TABLE climbing_data.membership_fees
ADD CONSTRAINT chk_amount CHECK (amount > 0);

ALTER TABLE climbing_data.climbers
ADD CONSTRAINT chk_postal_code CHECK (postal_code  > 0);

-- Check constraint to ensure end_date is after start_date
ALTER TABLE climbing_data.climbs
ADD CONSTRAINT chk_date_order CHECK (end_date > start_date);

-- Check constraint for valid payment methods (example)
ALTER TABLE climbing_data.membership_fees
ADD CONSTRAINT chk_payment_method CHECK (payment_method IN ('Credit Card', 'PayPal'));


--Ensure all personal information is always entered
ALTER TABLE climbing_data.climbers
ALTER COLUMN first_name SET NOT NULL,
ALTER COLUMN last_name SET NOT NULL,
ALTER COLUMN address SET NOT NULL,
ALTER COLUMN city SET NOT NULL,
ALTER COLUMN state SET NOT NULL,
ALTER COLUMN postal_code SET NOT NULL,
ALTER COLUMN country SET NOT NULL;

ALTER TABLE climbing_data.mountains
ALTER COLUMN name SET NOT NULL,
ALTER COLUMN height SET NOT NULL,
ALTER COLUMN country SET NOT NULL,
ALTER COLUMN region SET NOT NULL;

ALTER TABLE climbing_data.routes
ALTER COLUMN name SET NOT NULL,
ALTER COLUMN difficulty SET NOT NULL;

ALTER TABLE climbing_data.climbs
ALTER COLUMN start_date SET NOT NULL,
ALTER COLUMN end_date SET NOT NULL;

ALTER TABLE climbing_data.climb_equipment
ALTER COLUMN quantity SET NOT NULL;

ALTER TABLE climbing_data.membership_fees
ALTER COLUMN fee_date SET NOT NULL,
ALTER COLUMN amount SET NOT NULL,
ALTER COLUMN payment_method SET NOT NULL;

ALTER TABLE climbing_data.climber_achievements
ALTER COLUMN achievement_date SET NOT NULL,
ALTER COLUMN title SET NOT NULL,
ALTER COLUMN description SET NOT NULL,
ALTER COLUMN awarded_for SET NOT NULL;

ALTER TABLE climbing_data.expedition_photos
ALTER COLUMN photo_url SET NOT NULL,
ALTER COLUMN taken_by SET NOT NULL,
ALTER COLUMN taken_date SET NOT NULL,
ALTER COLUMN description SET NOT NULL;




INSERT INTO climbing_data.climbers (first_name, last_name, address, city, state, postal_code, country) 
VALUES 
('John', 'Smith', '123 Hill St', 'Highland', 'CA', 90001, 'USA'),
('Emily', 'Johnson', '234 Mountain Ave', 'Ridgeview', 'CO', 80021, 'USA');

INSERT INTO climbing_data.mountains (name, height, country, region) 
VALUES 
('Mount Everest', 8849, 'Nepal', 'Mahalangur'),
('Mount Kilimanjaro', 5895, 'Tanzania', 'Kilimanjaro');

INSERT INTO climbing_data.routes (mountain_id, name, difficulty) 
VALUES 
(1, 'Alpha Ascend', 'Moderate'),
(2, 'Zeta Challenge', 'Difficult');

INSERT INTO climbing_data.climbs (start_date, end_date) 
VALUES 
('2024-06-15', '2024-06-20'),
('2024-07-05', '2024-07-10');

INSERT INTO climbing_data.climber_climbs (climber_id, climb_id) 
VALUES 
(1, 101),
(2, 102);

INSERT INTO climbing_data.equipment (name, type, weight) 
VALUES 
('Climbing Rope', 'Safety', 10.5),
('Carabiners', 'Safety', 0.05),
('Harness', 'Gear', 1.0);

INSERT INTO climbing_data.climb_equipment (climb_id, equipment_id, quantity) 
VALUES 
(101, 401, 2),
(101, 402, 4),
(102, 401, 3),
(102, 402, 1);

INSERT INTO climbing_data.membership_fees (climber_id, fee_date, amount, payment_method) 
VALUES 
(1, '2024-01-10', 100.00, 'Credit Card'),
(2, '2024-01-15', 100.00, 'PayPal');

INSERT INTO climbing_data.climber_achievements (climber_id, achievement_date, title, description, awarded_for) 
VALUES 
(1, '2024-06-21', 'Everest Summit', 'Reached the summit of Mount Everest', 'Climbing Everest'),
(2, '2024-07-11', 'Kilimanjaro Summit', 'Reached the summit of Mount Kilimanjaro', 'Climbing Kilimanjaro');

INSERT INTO climbing_data.expedition_photos (climb_id, photo_url, taken_by, taken_date, description) 
VALUES 
(101, 'http://photos.example.com/everest1.jpg', 'John Smith', '2024-06-20', 'Summit view on Mount Everest'),
(102, 'http://photos.example.com/kili2.jpg', 'Emily Johnson', '2024-07-10', 'Sunrise over Kilimanjaro');

INSERT INTO climbing_data.climb_routes (climb_id, route_id) 
VALUES 
(101, 401),
(102, 402);




-- Add record_ts columns to each table with a default current date
ALTER TABLE climbing_data.climbers ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.mountains ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.routes ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.climbs ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.climber_climbs ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.equipment ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.climb_equipment ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.membership_fees ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.climber_achievements ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.expedition_photos ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE climbing_data.climb_routes ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

SELECT * FROM climbing_data.climbers;
SELECT * FROM climbing_data.mountains;
SELECT * FROM climbing_data.routes;
SELECT * FROM climbing_data.climbs;
SELECT * FROM climbing_data.climber_climbs;
SELECT * FROM climbing_data.equipment;
SELECT * FROM climbing_data.climb_equipment;
SELECT * FROM climbing_data.membership_fees;
SELECT * FROM climbing_data.climber_achievements;
SELECT * FROM climbing_data.expedition_photos;
SELECT * FROM climbing_data.climb_routes;




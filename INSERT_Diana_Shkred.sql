
--Choose one of your favorite films and add it to the "film" table. Fill in rental rates with 4.99 and rental durations with 2 weeks.
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features, fulltext)
  VALUES ('Interstellar', 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity survival.', 2014, 1, 14, 4.99, 169, 19.99, 'PG-13', NOW(), '{Behind the Scenes, Trailers}', 'interstellar team explorers travel wormhole space attempt ensure humanity survival');


--Add the actors who play leading roles in your favorite film to the "actor" and "film_actor" tables (three or more actors in total).
INSERT INTO actor (first_name, last_name) VALUES ('Matthew', 'McConaughey');
INSERT INTO actor (first_name, last_name) VALUES ('Anne', 'Hathaway');
INSERT INTO actor (first_name, last_name) VALUES ('Jessica', 'Chastain');
INSERT INTO actor (first_name, last_name) VALUES ('Michael', 'Caine');


INSERT INTO film_actor (actor_id, film_id)
SELECT actor_id, (SELECT film_id FROM film WHERE title = 'Interstellar')
FROM actor
WHERE (first_name = 'Matthew' AND last_name = 'McConaughey')
   OR (first_name = 'Anne' AND last_name = 'Hathaway')
   OR (first_name = 'Jessica' AND last_name = 'Chastain')
   OR (first_name = 'Michael' AND last_name = 'Caine');
 
--Add your favorite movies to any store's inventory.
INSERT INTO inventory (film_id, store_id, last_update)
SELECT film_id, 1, NOW()
FROM film
WHERE title = 'Interstellar';



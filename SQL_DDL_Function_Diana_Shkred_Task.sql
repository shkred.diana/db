/*Create a view called "sales_revenue_by_category_qtr" that shows the film category and total sales revenue for the current quarter. 
 The view should only display categories with at least one sale in the current quarter. The current quarter should be determined dynamically.*/
 
-- Drop the existing view if it exists
DROP VIEW IF EXISTS sales_revenue_by_category_qtr;

CREATE VIEW sales_revenue_by_category_qtr AS
WITH current_quarter AS (
    SELECT 
        EXTRACT(YEAR FROM CURRENT_DATE) AS year,
        CEIL(EXTRACT(MONTH FROM CURRENT_DATE) / 3) AS quarter
),
sales AS (
    SELECT 
        p.payment_id,
        p.amount,
        f.film_id,
        fc.category_id,
        p.payment_date
    FROM 
        payment p
    JOIN rental r ON p.rental_id = r.rental_id
    JOIN inventory i ON r.inventory_id = i.inventory_id
    JOIN film f ON i.film_id = f.film_id
    JOIN film_category fc ON f.film_id = fc.film_id
),
filtered_sales AS (
    SELECT 
        s.category_id,
        s.amount,
        s.payment_date
    FROM 
        sales s,
        current_quarter cq
    WHERE 
        EXTRACT(YEAR FROM s.payment_date) = cq.year
        AND CEIL(EXTRACT(MONTH FROM s.payment_date) / 3) = cq.quarter
),
total_sales_by_category AS (
    SELECT 
        fc.name AS category,
        SUM(fs.amount) AS total_revenue
    FROM 
        filtered_sales fs
    JOIN category fc ON fs.category_id = fc.category_id
    GROUP BY 
        fc.name
)
SELECT 
    category,
    total_revenue
FROM 
    total_sales_by_category
WHERE 
    total_revenue > 0;

   SELECT * FROM sales_revenue_by_category_qtr;



/*Create a query language function called "get_sales_revenue_by_category_qtr" that accepts one parameter representing 
the current quarter and returns the same result as the "sales_revenue_by_category_qtr" view.*/

DROP VIEW IF EXISTS sales_revenue_by_category_qtr;

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr()
RETURNS TABLE(category text, total_revenue numeric) AS $$
BEGIN
    RETURN QUERY
    WITH current_quarter AS (
        SELECT 
            EXTRACT(YEAR FROM CURRENT_DATE) AS year,
            CEIL(EXTRACT(MONTH FROM CURRENT_DATE) / 3) AS quarter
    ),
    sales AS (
        SELECT 
            p.payment_id,
            p.amount,
            f.film_id,
            fc.category_id,
            p.payment_date
        FROM 
            payment p
        JOIN rental r ON p.rental_id = r.rental_id
        JOIN inventory i ON r.inventory_id = i.inventory_id
        JOIN film f ON i.film_id = f.film_id
        JOIN film_category fc ON f.film_id = fc.film_id
    ),
    filtered_sales AS (
        SELECT 
            s.category_id,
            s.amount,
            s.payment_date
        FROM 
            sales s,
            current_quarter cq
        WHERE 
            EXTRACT(YEAR FROM s.payment_date) = cq.year
            AND CEIL(EXTRACT(MONTH FROM s.payment_date) / 3) = cq.quarter
    ),
    total_sales_by_category AS (
        SELECT 
            fc.name AS category,
            SUM(fs.amount) AS total_revenue
        FROM 
            filtered_sales fs
        JOIN category fc ON fs.category_id = fc.category_id
        GROUP BY 
            fc.name
    )
    SELECT 
        total_sales_by_category.category,
        total_sales_by_category.total_revenue
    FROM 
        total_sales_by_category
    WHERE 
        total_sales_by_category.total_revenue > 0;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM get_sales_revenue_by_category_qtr();






/*Create a procedure language function called "new_movie" that takes a movie title as a parameter and inserts a new movie with the given title in the film table. 
The function should generate a new unique film ID, set the rental rate to 4.99, the rental duration to three days, the replacement cost to 19.99, 
the release year to the current year, and "language" as Klingon. The function should also verify that the language exists in the "language" table. 
Then, ensure that no such function has been created before; if so, replace it.*/

DROP FUNCTION IF EXISTS new_movie(text);

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM language WHERE name = 'Klingon') THEN
        INSERT INTO language (name, last_update)
        VALUES ('Klingon', CURRENT_TIMESTAMP);
    END IF;
END $$;



CREATE OR REPLACE FUNCTION new_movie(movie_title text)
RETURNS void AS $$
DECLARE
    new_film_id int;
    klingon_language_id int;
    current_year int := EXTRACT(YEAR FROM CURRENT_DATE);
BEGIN
    -- Verify that "Klingon" exists in the language table
    SELECT language_id INTO klingon_language_id
    FROM language
    WHERE name = 'Klingon';

    -- If "Klingon" does not exist, raise an exception
    IF klingon_language_id IS NULL THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table.';
    END IF;

    -- Generate a new unique film ID
    SELECT nextval('film_film_id_seq') INTO new_film_id;

    -- Insert the new movie into the film table
    INSERT INTO film (film_id, title, release_year, language_id, rental_duration, rental_rate, replacement_cost, last_update)
    VALUES (new_film_id, movie_title, current_year, klingon_language_id, 3, 4.99, 19.99, CURRENT_TIMESTAMP);

END;
$$ LANGUAGE plpgsql;

SELECT new_movie('Your Movie Title');

